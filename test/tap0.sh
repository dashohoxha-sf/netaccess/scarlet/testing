#!/bin/bash

/sbin/ip link set tap0 up
/sbin/ip address flush dev tap0
/sbin/ip address add 192.168.100.1/24 dev tap0
/sbin/ip address add 10.0.0.10/24 dev tap0

#!/bin/bash
### Start scarlet with two network cards.
### Note: it should be run by root, so that tapX interfaces can be created.

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### emulate a testbox with two network cards
qemu -m 64 -serial /dev/tty8 images/scarlet.img \
     -net nic,vlan=0  -net nic,vlan=1  \
     -net tap,vlan=0,ifname=tap0,script=./tap0.sh \
     -net tap,vlan=1,ifname=tap1,script=./tap1.sh  &
     #-net socket,vlan=0,listen=:1234 \


#!/bin/bash

/sbin/ip link set tap1 up
/sbin/ip address flush dev tap1
/sbin/ip address add 192.168.10.101/24 dev tap1

#!/bin/bash
### Start the emulation of the gateway/router testing environment.
### Note: it should be run by root, so that the firewall can be modified
###       and so that tapX interfaces can be created.

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### get the system to be tested
testbox=${1:-images/scarlet.img}

### go to this directory
cd $(dirname $0)

### accept in firewall tapX and enable source NAT
./firewall.sh enable

### emulate a testbox with three network cards
qemu -kernel-kqemu -m 64 -serial /dev/tty8 $testbox \
     -net nic,vlan=0  -net nic,vlan=1  -net nic,vlan=2  \
     -net socket,vlan=0,listen=:1234 \
     -net tap,vlan=0,ifname=tap01,script=./tap01-setup.sh \
     -net tap,vlan=1,ifname=tap1,script=./tap1-setup.sh   \
     -net tap,vlan=2,ifname=tap2,script=./tap2-setup.sh &

### configure a route to ip 10.0.0.1 of tap01
sleep 10
./network.sh

### wait until the first testbox boots up
sleep 30

### emulate a second testbox with two network cards, 
### which will be gateway for the first testbox
qemu -kernel-kqemu -m 64 images/wan.img \
     -net nic,vlan=0,macaddr=52:54:00:12:34:60 \
     -net nic,vlan=1,macaddr=52:54:00:12:34:61 \
     -net tap,vlan=0,ifname=tap02,script=./tap02-setup.sh \
     -net socket,vlan=1,connect=127.0.0.1:1234

### close the firewall for tapX and disable source NAT
./firewall.sh disable


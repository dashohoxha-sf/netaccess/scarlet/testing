#!/bin/bash

### include the config file
cd $(dirname $0)
.  network.cfg

### setup the interface
/sbin/ip link set tap01 up
/sbin/ip address flush dev tap01
/sbin/ip address add $HOST_LAN_IP1 dev tap01
/sbin/ip address add $ADMIN_IP dev tap01

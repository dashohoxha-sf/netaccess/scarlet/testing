#!/bin/bash

### include the config file
cd $(dirname $0)
.  network.cfg

### setup eth0
/sbin/ip link set dev eth0 up
/sbin/ip address flush dev eth0
/sbin/ip address add $HOST_WAN_IP dev eth0

### add a default route
/sbin/ip route add to default via $HOST_GW

### setup different routing (gateway) for packets 
### coming from clients (out of interfaces tap1 and tap2);
### these packets are marked with the mark 10 by the firewall
sed -e "/201  clients/d" -i /etc/iproute2/rt_tables
echo "201  clients" >> /etc/iproute2/rt_tables

/sbin/ip rule del fwmark 10 table clients 2> /dev/null 
/sbin/ip rule add fwmark 10 table clients

/sbin/ip route del default via $HOST_GW dev eth0 table clients 2> /dev/null
/sbin/ip route add default via $HOST_GW dev eth0 table clients

### these are added in order to access a real scarlet box in the LAN
/sbin/ip route del 10.0.0.0/24 dev tap01
/sbin/ip route del to 10.0.0.1/32 dev tap01
/sbin/ip route add to 10.0.0.1/32 dev tap01 scope link  src 10.0.0.10
/sbin/ip route del to 10.0.0.2/32 dev tap01
/sbin/ip route add to 10.0.0.2/32 dev tap01 scope link  src 10.0.0.10
/sbin/ip address add 10.0.0.11/24 dev eth0


#!/bin/bash

### include the config file
cd $(dirname $0)
.  network.cfg

### setup the interface
/sbin/ip link set tap02 up
/sbin/ip address flush dev tap02
/sbin/ip address add $HOST_LAN_IP2 dev tap02

#!/bin/bash
### accept/not-accept input from tapX and
### enable or disable source NAT (masquerading) for tap0

function usage
{
  echo "Usage: ${0} [enable | disable | list]"
  exit
}

### check that there is one parameter and get it
if [ -z $1 ]; then usage; fi
ACTION=$1

### include the config file
cd $(dirname $0)
.  network.cfg

### variables
WAN_TAPS="tap01 tap02"
LAN_TAPS="tap1 tap2"
ALL_TAPS="$WAN_TAPS $LAN_TAPS"
IPT=/sbin/iptables

function insert_rules
{
  ### mark packets comming from wan tap interfaces
  for tap in $WAN_TAPS
  do
    $IPT --table mangle --insert PREROUTING \
         --in-interface $tap --jump MARK --set-mark 10
  done

  ### accept input from the tap interfaces
  for tap in $ALL_TAPS
  do
    $IPT --table filter --insert INPUT --in-interface $tap --jump ACCEPT
  done

  ### accept forward from the wan tap interfaces
  for tap in $WAN_TAPS
  do
    $IPT --table filter --insert FORWARD --in-interface $tap --jump ACCEPT
  done

  ### enable forwarding in the kernel
  echo 1 > /proc/sys/net/ipv4/ip_forward

  ### enable source NAT
  /sbin/iptables --table nat --insert POSTROUTING \
                 --out-interface eth0 --jump MASQUERADE
}

function delete_rules
{
  ### delete marking rules for packets comming from wan tap interfaces
  for tap in $WAN_TAPS
  do
    $IPT --table mangle --delete PREROUTING \
         --in-interface $tap --jump MARK --set-mark 10
  done

  ### delete input rules for tap interfaces
  for tap in $ALL_TAPS
  do
    $IPT --table filter --delete INPUT --in-interface $tap --jump ACCEPT
  done

  ### delete forward rules for wan tap interfaces
  for tap in $WAN_TAPS
  do
    $IPT --table filter --delete FORWARD --in-interface $tap --jump ACCEPT
  done

  ### disable forwarding in the kernel
  echo 0 > /proc/sys/net/ipv4/ip_forward

  ### disable source NAT
  /sbin/iptables --table nat --delete POSTROUTING \
                 --out-interface eth0 --jump MASQUERADE
}

### list the existing rules
function list-rules
{
  /sbin/iptables-save | grep -E 'MASQUERADE|tap'
}

case $ACTION in
  enable  )  insert_rules ;;
  disable )  delete_rules ;;
  *       )  list-rules ;;
esac


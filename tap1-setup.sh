#!/bin/bash

### include the config file
cd $(dirname $0)
.  network.cfg

### setup the interface
/sbin/ip link set tap1 up
/sbin/ip address flush dev tap1
/sbin/ip address add $CLIENT1_IP dev tap1

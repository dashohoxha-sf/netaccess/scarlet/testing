#!/bin/bash
### Sets the role of the main computer (by changing the default route etc.).

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### include the config file
cd $(dirname $0)
.  network.cfg

function usage()
{
  echo "Usage: $0 [ls | normal | client1 | client2]"
  echo "Sets the role of the HOST (by changing the default route etc.)."
  exit 0
}

function show_current_role
{
  gateway=$(/sbin/ip route | grep default | cut -d' ' -f3)
  case $gateway in
    $HOST_GW ) echo "normal" ;;
    $LAN1_GW ) echo "client1" ;;
    $LAN2_GW ) echo "client2" ;;
    * )        echo "unknown, gateway=$gateway" ;;
  esac
}

function set_normal
{
  ### delete the table clients
  /sbin/ip route flush table clients

  ### change the default route
  /sbin/ip route del default 2> /dev/null
  /sbin/ip route add default via $HOST_GW

  ### change resolv.conf as well
  echo "nameserver $HOST_GW" > /etc/resolv.conf
}

function set_client1
{
  ### copy all routes of eth0 to table 'clients'
  /sbin/ip route flush table clients
  /sbin/ip route show | grep eth0 \
    | while read ROUTE ; do /sbin/ip route add $ROUTE table clients ; done

  ### change the default routes
  /sbin/ip route del default 2> /dev/null
  /sbin/ip route add default via $LAN1_GW
  /sbin/ip route del default table clients 2> /dev/null
  /sbin/ip route add default via $HOST_GW table clients

  ### change resolv.conf as well
  echo "nameserver $LAN1_GW" > /etc/resolv.conf
}

function set_client2
{
  ### copy all routes of eth0 to table 'clients'
  /sbin/ip route flush table clients
  /sbin/ip route show | grep eth0 \
    | while read ROUTE ; do /sbin/ip route add $ROUTE table clients ; done

  ### change the default routes
  /sbin/ip route del default 2> /dev/null
  /sbin/ip route add default via $LAN2_GW
  /sbin/ip route del default table clients 2> /dev/null
  /sbin/ip route add default via $HOST_GW table clients

  ### change resolv.conf as well
  echo "nameserver $LAN2_GW" > /etc/resolv.conf
}


if [ "$1" = "" ]; then usage; fi
role=$1

case $role in
  ls      )  show_current_role ;;
  normal  )  set_normal ;;
  client1 )  set_client1 ;;
  client2 )  set_client2 ;;
  *       )  usage ;;
esac


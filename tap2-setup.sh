#!/bin/bash

### include the config file
cd $(dirname $0)
.  network.cfg

### setup the interface
/sbin/ip link set tap2 up
/sbin/ip address flush dev tap2
/sbin/ip address add $CLIENT2_IP dev tap2
